from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy 
from datetime import datetime
from flask_migrate import Migrate
migrate = Migrate(compare_type=True)

app = Flask(__name__)
migrate.init_app(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///blog.db'

db = SQLAlchemy(app)



class Blogpost(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    subtitle = db.Column(db.String(50))
    author = db.Column(db.String(50))
    date_posted = db.Column(db.DateTime)
    content = db.Column(db.Text)



@app.route('/')
def index():
    posts = Blogpost.query.order_by(Blogpost.date_posted.desc()).all()
    

    return render_template('index.html', posts=posts)
    
@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Blogpost.query.filter_by(id=post_id).one()
    post.date_posted = post.date_posted.strftime("%b %d %Y")
    return render_template('post.html', post=post)

@app.route('/contact')
def contact():
    return render_template('contact.html')

@app.route('/add-post')
def add_post():
    #return"this"
    return render_template('add-post.html')

@app.route('/submit', methods=['POST'])
def submit():

    if request.method == 'POST':
        title  = request.form['title']
        subtitle = request.form['subtitle']
        author = request.form['author']
        content = request.form['content']
        
        post = Blogpost(title=title, subtitle=subtitle, author=author, content=content, date_posted=datetime.now())
        db.session.add(post)
        db.session.commit()

        
        return redirect(url_for('index'))





if __name__ == '__main__':
    app.run(debug=True)